const HtmlWebpackPlugin = require('html-webpack-plugin');
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const { dependencies } = require("./package.json");

module.exports = {
    mode: 'development',
    devServer: {
      port:5002,
      compress: true,
      allowedHosts: "all",
    },
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                use: {
                  loader: 'babel-loader',
                  options: {
                    presets: ['@babel/preset-react', '@babel/preset-env'],
                    plugins: ['@babel/plugin-transform-runtime'],
                  },
                },
              },
              {
                test: /\.?css$/,
                use: [
                  'style-loader',
                  'css-loader',
                  'sass-loader'
                ]
              }
        ]
    },
    plugins: [
          new ModuleFederationPlugin({
            name: "Mfe2Remote",
            filename: "moduleEntry.js",
            exposes: {
              "./App": "./src/App",
            },
            shared: {
              ...dependencies,
              react: {
                singleton: true,
                requiredVersion: dependencies["react"],
              },
              "react-dom": {
                singleton: true,
                requiredVersion: dependencies["react-dom"],
              },
            },
        }),
        new HtmlWebpackPlugin({
          template: './public/index.html',
        }),
      ]    
  };